
var app = {
    // Application Constructor
    initialize: function () {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        var takePhoto = document.getElementById('takePhoto');
        takePhoto.addEventListener('click', app.takePhoto, false);
        $("#formtest").submit(function (event) {
            event.preventDefault();
            //var data = JSON.stringify($("#formtest").serializeArray());
            var data = JSON.stringify(
                {
                    id: $("#id").val(),
                    nombre: $("#nombre").val(),
                    nacionalidad: $("#nacionalidad").val(),
                    edad: $("#edad").val(),
                    genero: $("input[name='genero']:checked").val(),
                    rut: $("#rut").val(),
                    esRut: $("input[name='esRut']:checked").val(),
                    email: $("#email").val(),
                    fono: $("#fono").val(),
                    password: $("#password").val(),
                    foto: $("#formPhoto").val(),
                }
            );
            alert(data);
            if ($("input#formPhoto").val() == "") {
                alert("No se ha detectado una foto");
            }
            else {
                $.ajax({
                    url: "http://192.168.0.23:8080/demo/add",
                    data: data,
                    type: "POST",
                    contentType:"application/json",
                    success: function (result) { alert(result); },
                    error: function (xhr) {
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    }
                });
            }
        });
        $("#readusers").submit(function (event) {
            event.preventDefault();
            var template1 = "<div id='";
            var template2 = "' class='row'><div class='col-6'>Nombre: '";
            var template3 = "' </div><div class='col-6'><img src='data:image/jpeg;base64,";
            var template4 = "'></img></div></div>";
            $.ajax({
                url: "http://192.168.0.23:8080/demo/all",
                data: "",
                type: "GET",
                success: function (result) {
                    $("#inscritostable").empty();
                    for (var i = 0; i < result.length; i++) {
                        $("#inscritostable").append(template1 + i + template2 + result[i].nombre+ template3 + result[i].foto + template4);
                    }                
                },
                error: function (xhr) {
                    alert("An error occured: " + xhr.status + " " + xhr.statusText);
                }
            });
        });
    },
    //funcion legado
    sendPrueba: function () {
        var b = document.getElementById("response");
        b.innerHTML = "test";
        $.ajax({
            url: "http://192.168.0.23:8080/demo/add",
            data: '{"nombre":"prueba"}',
            type: "POST",
            success: function (result) { alert(result.d); },
            error: function (xhr) {
                alert("An error occured: " + xhr.status + " " + xhr.statusText);
            }
        });
    },

    sendPhoto: function () {
        alert('Imagen enviada al servidor');
    },

    takePhoto: function () {
        navigator.camera.getPicture(app.onPhotoDataSuccess, app.onFail, {
            quality: 75,
            allowEdit: true, destinationType: navigator.camera.DestinationType.DATA_URL, targetHeight: 240, targetWidth: 240
        });
    },
    onPhotoDataSuccess: function (imageData) {

        var photo = document.getElementById('photo');

        photo.style.display = 'block';

        photo.src = "data:image/jpeg;base64," + imageData;
        var formulario = document.getElementById("formPhoto");
        formulario.value = imageData;


    },

    onFail: function (message) {
        alert('Failed because: ' + message);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function () {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function (id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};
